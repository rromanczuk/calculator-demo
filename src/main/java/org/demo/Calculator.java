package org.demo;

// Precedence climbing calculator
public class Calculator {

    String[] tokens;
    private int position;

    public Calculator(String expression) {
        tokens = expression.trim().split(" ");
        position = 0;
    }

    public double calculate() {
        return consumeExpression(1);
    }

    public double consumeExpression(int minPrecedence) {
        double result = consumeInteger();

        while (position < tokens.length && peekOperator().getPrecedence() >= minPrecedence) {
            Operator operator = consumeOperator();

            int nextMinPrecedence;
            if (operator.getAssociativity().equals(Operator.Associativity.LEFT)) {
                nextMinPrecedence = operator.getPrecedence() + 1;
            } else {
                nextMinPrecedence = operator.getPrecedence();
            }

            double rhs = consumeExpression(nextMinPrecedence);

            switch (operator) {
                case MULTIPLICATION -> result *= rhs;
                case DIVISION -> {
                    if (rhs == 0) {
                        throw new ArithmeticException("Division by 0");
                    }
                    result /= rhs;
                }
                case PLUS -> result += rhs;
                case MINUS -> result -= rhs;
                default -> throw new IllegalArgumentException("Unknown operator");
            }
        }

        return result;
    }

    private Operator peekOperator() {
        return Operator.getOperatorForToken(tokens[position])
                       .orElseThrow(() -> new IllegalArgumentException("Unknown operator"));
    }

    private Operator consumeOperator() {
        return Operator.getOperatorForToken(tokens[position++])
                       .orElseThrow(() -> new IllegalArgumentException("Unknown operator"));
    }

    private int consumeInteger() {
        try {
            return Integer.parseInt(tokens[position++]);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Invalid arithmetic expression");
        }
    }
}
