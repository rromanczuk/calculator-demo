package org.demo;

import java.util.Arrays;
import java.util.Optional;

public enum Operator {
    PLUS("+", 1, Associativity.LEFT),
    MINUS("-", 1, Associativity.LEFT),
    MULTIPLICATION("*", 2, Associativity.LEFT),
    DIVISION("/", 2, Associativity.LEFT);

    enum Associativity {
        LEFT, RIGHT
    }

    private final String token;
    private final int precedence;
    private final Associativity associativity;

    Operator(String operator, int precedence, Associativity associativity) {
        this.token = operator;
        this.precedence = precedence;
        this.associativity = associativity;
    }

    public int getPrecedence() {
        return precedence;
    }

    public Associativity getAssociativity() {
        return associativity;
    }

    public static Optional<Operator> getOperatorForToken(String token) {
        return Arrays.stream(Operator.values())
                     .filter(o -> o.token.equals(token))
                     .findFirst();
    }
}
