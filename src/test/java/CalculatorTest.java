import org.demo.Calculator;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@DisplayName("Calculator tests")
class CalculatorTest {

    @DisplayName("Valid expressions")
    @ParameterizedTest(name = "{index} ==> expression ''{0}'' result is {1}")
    @CsvSource({
            "2 + 2, 4",
            "4 - 2, 2",
            "4 - -2, 6",
            "4 * 2, 8",
            "4 / 2, 2",
            "-2 / 4, -0.5",
            "2 * 3 + 7, 13",
            "2 + 3 * 7, 23",
            "2 + -12 * 7, -82",
            "2 + 6 / 3, 4",
            "4, 4"
    })
    void validExpressions(String expression, double result) {
        Calculator calculator = new Calculator(expression);

        assertThat(calculator.calculate()).isEqualTo(result);
    }

    @DisplayName("Invalid expressions")
    @ParameterizedTest(name = "{index} ==> expression ''{0}'' throws IllegalArgumentException")
    @ValueSource(strings = {
            "",
            " ",
            ",",
            "+",
            "*",
            "a",
            "2 + b",
            "2 ^ 2"
    })
    void invalidExpression(String expression) {
        Calculator calculator = new Calculator(expression);

        assertThrows(IllegalArgumentException.class, calculator::calculate);
    }

    @DisplayName("Invalid arithmetic")
    @ParameterizedTest(name = "{index} ==> expression ''{0}'' throws ArithmeticException")
    @ValueSource(strings = {
            "0 / 0"
    })
    void invalidArithmetic(String expression) {
        Calculator calculator = new Calculator(expression);

        assertThrows(ArithmeticException.class, calculator::calculate);
    }
}
